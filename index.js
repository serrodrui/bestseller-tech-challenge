const https = require('https');

const api = 'https://en.wikipedia.org/w/api.php';

function getArticleInfo(pageName = 'Bestseller_(company)') {
  return new Promise((resolve, reject) => {
    https.get(`${api}?action=parse&format=json&formatversion=2&page=${pageName}`, (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        resolve(JSON.parse(data).parse);
      });

    }).on("error", (err) => {
      reject(`Error: ${err.message}`);
    });
  })
}

function displayCategories(categories = []) {
  categories.filter((categoryItem) => {
    !categoryItem.hidden && console.log(categoryItem.category.replace(/_/g, " "));
  });
}

function displayToc(sections = []) {
  sections.forEach((sectionItem) => {
    const tabs = '\t'.repeat(sectionItem.toclevel - 1);
    console.log(`${tabs}${sectionItem.number} ${sectionItem.line}`);
  });
}

function displayImages(articleText = '') {
  const regex = /<img[^>]+src="(?<src>[^">]+)"/g;
  while (image = regex.exec(articleText)) {
    console.log(image.groups.src);
  }
}

(async () => {
  try {
    const pageName = process.argv.slice(2)[0];
    const ArticleInfo = await getArticleInfo(pageName);

    console.log(ArticleInfo.title);
    console.log('\nCategories: \n');
    displayCategories(ArticleInfo.categories);
    console.log('\nContents: \n');
    displayToc(ArticleInfo.sections);
    console.log('\nImages: \n');
    displayImages(ArticleInfo.text);
  } catch (error) {
    console.error(error);
  }
})()
