# BESTSELLER – Full-Stack developer challenge

## How to run this?
```sh
  node index [Wikipedia-page-name]
```

```sh
  node index Amsterdam
```

Default page is: Bestseller_(company)

Note: Special characters may need to be escaped

```sh
  node index "Bestseller_(company)"
```
